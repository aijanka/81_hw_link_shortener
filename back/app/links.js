const express = require('express');
const router = express.Router();
const nanoid = require('nanoid');
const Link = require('../models/Link');

const createRouter = () => {
    router.post('/links', (req, res) => {


        const link = new Link(req.body);
        link.shortLink = nanoid(6);
        // не знаю, зачем это???
        link.save()
            .then(result => res.send(result))
            .catch(() => res.status(400).send());

    //     db.collection('links').insertOne(link)
    //         .then(result => {
    //             console.log(result.ops[0], 'in createRouter');
    //             res.send(result.ops[0]);
    //         })
     })

    router.get('/:shortUrl', (req, res) => {
        console.log(req.params.shortUrl);
        Link.findOne({shortLink: req.params.shortUrl})
            .then(result => {
                if(result) res.status(301).send(result.originalLink);
            })
            .catch(error => res.status(500).send(error));
    })
    return router;
};

module.exports = createRouter;