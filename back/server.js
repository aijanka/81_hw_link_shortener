const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();

const port = 8000;
const config = require('./config');
const links = require('./app/links');


app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

db.once("open", db => {
    console.log('Connected to MongoDB!');

   app.use('/', links(db));

    app.listen(port);
})

