const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    originalLink: {
        type: String, required: true
    },
    shortLink: {
        type: String, required: true
    }
});

const Link = mongoose.model('Link', Schema);

module.exports = Link;