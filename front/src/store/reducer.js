import {GET_SUCCESS, SHORTEN_REQUEST, SHORTEN_SUCCESS} from "./actionTypes";

const initialState = {
    originalLink: '',
    shortLink: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case SHORTEN_REQUEST:
            return {...state, originalLink: action.link};
        case SHORTEN_SUCCESS:
            return {...state, shortLink: action.shortenedLink};
        case GET_SUCCESS:
            return {...state, originalLink: action.originalLink};
        default:
            return state;
    }
};

export default reducer;