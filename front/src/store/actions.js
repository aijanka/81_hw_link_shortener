import axios from 'axios';
import {SHORTEN_REQUEST, SHORTEN_SUCCESS} from "./actionTypes";


export const shortenRequest = link => ({type: SHORTEN_REQUEST, link});
export const shortenSuccess = shortenedLink => ({type: SHORTEN_SUCCESS, shortenedLink});

export const shortenLink = link => {
    return dispatch => {
        dispatch(shortenRequest(link.originalLink));
        axios.post('/links', link).then(response => {
            console.log(response);
            dispatch(shortenSuccess(response.data.shortLink));
        })
    }
};
