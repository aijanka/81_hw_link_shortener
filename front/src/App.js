import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import {shortenLink} from "./store/actions";

class App extends Component {
    state = {
        originalLink: '',
    };

    saveInputChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    showShortenedLink = event => {
        event.preventDefault();
        this.props.shortenLink(this.state);
    }

    render() {
        return (
            <div className="App">
                <h1>Shorten your link!</h1>
                <input
                    type="text"
                    placeholder='Enter your URL here'
                    name="originalLink"
                    onChange={this.saveInputChange}
                    value={this.state.originalLink}
                />
                <button onClick={this.showShortenedLink}>Shorten!</button>
                {this.props.shortLink
                    ?
                    <div>
                        <h4>Your link now looks like this: </h4>
                        <a href={`http://localhost:8000/${this.props.shortLink}`}>{'http://localhost:8000/' + this.props.shortLink}</a>
                    </div>
                    : null}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    originalLink: state.originalLink,
    shortLink: state.shortLink
});

const mapDispatchToProps = dispatch => ({
    // getShortLink: () => dispatch(getLink()),
    shortenLink: (link) => dispatch(shortenLink(link))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
