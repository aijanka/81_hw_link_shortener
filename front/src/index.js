import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';
import {applyMiddleware, createStore} from "redux";
import thunk from 'redux-thunk';
import reducer from "./store/reducer";
import {Provider} from "react-redux";

axios.defaults.baseURL = 'http://localhost:8000';
const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
